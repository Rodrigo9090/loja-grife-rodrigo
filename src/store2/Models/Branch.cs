﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace store2.Models
{
    public class Branch
    {
        public int ID { get; set; }

        [Required]
        [StringLength(60, MinimumLength = 1)]
        [Display(Name = "Nome da Filial")]
        public string BranchName { get; set; }

        [Required]
        [StringLength(60, MinimumLength = 1)]
        [Display(Name = "Endereço")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Você deve fornecer um número de telefone")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Não é um número de telefone válido")]
        [Display(Name = "Telefone")]
        public string Phone { get; set; }
    }
}
