﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace store2.Models
{
    public class Item
    {
        public int ID { get; set; }

        public int SupplierID { get; set; }

        [Required]
        [StringLength(60, MinimumLength = 1)]
        [Display(Name = "Nome")]
        public string Name { get; set; }

        [Display(Name = "Detalhes")]
        public string Details { get; set; }

        [Range(1, 1000)]
        [DataType(DataType.Currency)]
        [Display(Name = "Preço")]
        public int Price { get; set; }

        
        [Display(Name = "Categoria")]
        public string Category { get; set; }

        public string Picture { get; set; }

        [Display(Name = "Data de Publicação")]
        public DateTime DateOfPublish { get; set; }

        public Supplier Supplier { get; set; }


    }
}
