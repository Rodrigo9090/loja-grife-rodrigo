﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace store2.Models
{
    public class Supplier
    {
        public int ID { get; set; }

        [Required]
        [StringLength(60, MinimumLength = 1)]
        public string Name { get; set; }

        [Required]
        [StringLength(60, MinimumLength = 1)]
        [Display(Name = "Nome")]
        public string ContactName { get; set; }

        [Required(ErrorMessage = "Você deve fornecer um número de telefone")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Não é um número de telefone válido")]
        [Display(Name = "Número de telefone")]
        public string PhoneNumber { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Endereço de email invalido")]
        public string Email { get; set; }

        public ICollection<Item> Items { get; set; }
    }
}
